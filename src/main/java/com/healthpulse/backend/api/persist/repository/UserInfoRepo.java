package com.healthpulse.backend.api.persist.repository;

import com.healthpulse.backend.api.persist.entity.UserInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInfoRepo extends JpaRepository<UserInfoEntity, Long> {
}
